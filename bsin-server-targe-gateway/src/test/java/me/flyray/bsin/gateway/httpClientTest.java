package me.flyray.bsin.gateway;


import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class httpClientTest {
    /**
     * get类型的
     *
     * @param url
     * @throws ClientProtocolException
     * @throws IOException
     */
    @Test
    public void testGet() throws ClientProtocolException, IOException {
        // 设置超时时间，单位是秒
        RequestConfig defaultRequestConfig = RequestConfig.custom().setSocketTimeout(3000).setConnectTimeout(3000)
                .setConnectionRequestTimeout(3000).build();
        CloseableHttpClient httpclient = HttpClients.createDefault();
        String url = "www.baidu.com";
        try {
            HttpGet httpget = new HttpGet(url);
            httpget.setConfig(defaultRequestConfig);
            ResponseHandler<String> responseHandler = new ResponseHandler<String>() {
                public String handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
                    int status = response.getStatusLine().getStatusCode();
                    if (status >= 200 && status < 300) {
                        HttpEntity entity = response.getEntity();
                        return entity != null ? EntityUtils.toString(entity) : null;
                    } else {
                        throw new ClientProtocolException("Unexpected response status: " + status);
                    }
                }
            };

            String responseBody = httpclient.execute(httpget, responseHandler);
            System.out.println(responseBody);
        } finally {
            httpclient.close();
        }
    }

    /**
     * form类型的，传参数
     *
     * @param url
     * @param map 参数
     * @throws ClientProtocolException
     * @throws IOException
     */
    @Test
    public void testPostParam() throws ClientProtocolException, IOException {
        // 设置超时时间
        RequestConfig defaultRequestConfig = RequestConfig.custom().setSocketTimeout(3000).setConnectTimeout(3000)
                .setConnectionRequestTimeout(3000).build();

        CloseableHttpClient httpClient = HttpClients.custom().setDefaultRequestConfig(defaultRequestConfig).build();
        httpClient = HttpClients.createDefault();
        Map<String, String> map = new HashMap<String, String>();
        map.put("arg", "/files/2");
        String url = "http://127.0.0.1:5001/api/v0/files/mkdir";
        HttpPost post = new HttpPost(url);
        post.setConfig(defaultRequestConfig);

        // 传参数
        List<BasicNameValuePair> list = new ArrayList<BasicNameValuePair>();
        for (Map.Entry<String, String> entry : map.entrySet()) {
            list.add(new BasicNameValuePair(entry.getKey(), entry.getValue()));
            System.out.println(String.format("key:%s, value:%s", entry.getKey().toString(), entry.getValue().toString()));
        }
        HttpEntity ent = new UrlEncodedFormEntity(list, "UTF-8");
        post.setEntity(ent);

        ResponseHandler<String> responseHandler = new ResponseHandler<String>() {
            public String handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
                int status = response.getStatusLine().getStatusCode();
                System.out.println(response.getEntity().toString());
                if (status >= 200 && status < 300) {
                    HttpEntity entity = response.getEntity();
                    return entity != null ? EntityUtils.toString(entity) : null;
                } else {
                    throw new ClientProtocolException("Unexpected response status: " + status);
                }
            }
        };

        System.out.println(httpClient.execute(post, responseHandler));
    }


    /**
     * post json
     *
     * @param url     请求地址
     * @param jsonStr 请求json字符串
     * @throws ClientProtocolException
     * @throws IOException
     */
    @Test
    public void testPostJson() throws ClientProtocolException, IOException {
        // 设置超时时间
        RequestConfig defaultRequestConfig = RequestConfig.custom().setSocketTimeout(3000).setConnectTimeout(3000)
                .setConnectionRequestTimeout(3000).build();

        CloseableHttpClient httpClient = HttpClients.custom().setDefaultRequestConfig(defaultRequestConfig).build();
        httpClient = HttpClients.createDefault();

        String url = "http://127.0.0.1:5001/api/v0/add";
        // 传json字符串
        String jsonStr = "{\"key\":\"value\"}";

        HttpPost post = new HttpPost(url);

        post.setConfig(defaultRequestConfig);

        StringEntity stringEntity = new StringEntity(jsonStr);
        stringEntity.setContentType("text/json");
        stringEntity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
        post.setEntity(stringEntity);

        ResponseHandler<String> responseHandler = new ResponseHandler<String>() {
            public String handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
                int status = response.getStatusLine().getStatusCode();
                if (status >= 200 && status < 300) {
                    HttpEntity entity = response.getEntity();
                    return entity != null ? EntityUtils.toString(entity) : null;
                } else {
                    throw new ClientProtocolException("Unexpected response status: " + status);
                }
            }
        };

        System.out.println(httpClient.execute(post, responseHandler));
    }

    /**
     * 上传文件   POST FILE
     *
     * @param url      上传接口地址
     * @param filePath
     * @param fileName
     * @throws ClientProtocolException
     * @throws IOException
     */
    @Test
    public void testPostFile() throws ClientProtocolException, IOException {
        RequestConfig defaultRequestConfig = RequestConfig.custom().setSocketTimeout(5000).setConnectTimeout(3000)
                .setConnectionRequestTimeout(3000).build();

        CloseableHttpClient httpClient = HttpClients.custom().setDefaultRequestConfig(defaultRequestConfig).build();
        httpClient = HttpClients.createDefault();
        String url = "http://127.0.0.1:5001/api/v0/add";
        String filePath = "/home/leonard/code/metadata/test.txt";
        String fileName = "test.txt";
        HttpPost post = new HttpPost(url);
        // 设置超时时间
        post.setConfig(defaultRequestConfig);

        // 传文件
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        builder.addBinaryBody("file", new File(filePath), ContentType.DEFAULT_BINARY, fileName);
        builder.addTextBody("name", "test");
        post.setEntity(builder.build());

        ResponseHandler<String> responseHandler = new ResponseHandler<String>() {
            public String handleResponse(HttpResponse response) throws ClientProtocolException, IOException {
                int status = response.getStatusLine().getStatusCode();
                if (status >= 200 && status < 300) {
                    HttpEntity entity = response.getEntity();
                    return entity != null ? EntityUtils.toString(entity) : null;
                } else {
                    throw new ClientProtocolException("Unexpected response status: " + status);
                }
            }
        };

        System.out.println(httpClient.execute(post, responseHandler));
    }


}
