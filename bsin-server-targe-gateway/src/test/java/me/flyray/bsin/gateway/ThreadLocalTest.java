package me.flyray.bsin.gateway;


import java.util.HashMap;
import java.util.Map;

public class ThreadLocalTest {
    // private static ThreadLocal<String> threadLocal = new ThreadLocal<>();

    private static final ThreadLocal<Map<String, Object>> threadLocal = new ThreadLocal<Map<String, Object>>() {
        protected Map<String, Object> initialValue() {
            return new HashMap<String, Object>();
        }
    };

    public static void set(String key, Object value) {
        Map<String, Object> map = threadLocal.get();
        if (map == null) {
            map = new HashMap<String, Object>();
            threadLocal.set(map);
        }
        map.put(key, value);
    }

    public static Object get(String key) {
        Map<String, Object> map = threadLocal.get();
        //threadLocal.remove();
        if (map == null) {
            map = new HashMap<String, Object>();
            threadLocal.set(map);
        }
        return map.get(key);
    }

    public static void main(String[] args) {
        Thread thread1 = new Thread(() -> {
            set("本地变量1", "11");
            set("本地变量11", "111");
            print("thread1");
            System.out.println("线程1的本地变量的值为:" + get("本地变量1"));
            System.out.println("线程11的本地变量的值为:" + get("本地变量11"));
        });

        Thread thread2 = new Thread(() -> {
            set("本地变量2", "22");
            set("本地变量22", "222");
            print("thread2");
            System.out.println("线程2的本地变量的值为:" + get("本地变量1"));
            System.out.println("线程22的本地变量的值为:" + get("本地变量11"));
        });

        thread1.start();
        thread2.start();
    }

    public static void print(String s) {
        System.out.println(s + ":" + threadLocal.get());

    }
}
