# 流程引擎
[toc]

## 问题
* Could not update Flowable database schema: unknown version from database: '6.6.0'

修改表 act_ge_property ACT_ID_PROPERTY

## 模型保存存在多个版本
* 部署的时候根据版本去部署，部署成最新的定义

## 部署之后在表 act_re_deployment? act_re_procdef 中会存在数据

* 每次部署act_re_procdef中的版本都会增加

### 流程部署表中的数据很多为空，确认源码部署中是否为空

## 启动流程实例
* 根据key启动流程实例

## flowable是命令（指令）模式
* 根据指令找到指令中的execute方法跟踪执行逻辑

## 关注businessKey字段

## 流程中表单获取
* 在获取表单时，将表单分成了三类： 一个是开始表单，一个是历史节点表单，一个是当前节点表单
* 开始表单通过 formService.getStartFormData 获取
* 历史节点表单数据通过 historyService 提供的接口获取
* 当前节点的表单通过 formService.getTaskFormData 获取

## 待处理问题
* 倒序排序

## 端口说明
sofa rest 默认端口 8341
