package me.flyray.bsin.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;

/**
 * @author ：bolei
 * @date ：Created in 2021/11/30 16:00
 * @description：bsin 脚手架启动类
 * @modified By：
 */

@ImportResource({"classpath*:sofa/rpc-provider-hello.xml"})
@ComponentScan("me.flyray.bsin.*")
@SpringBootApplication
public class BsinWorkflowApplication {

    public static void main(String[] args) {
        SpringApplication springApplication = new SpringApplication(BsinWorkflowApplication.class);
        springApplication.run(args);
    }

}
