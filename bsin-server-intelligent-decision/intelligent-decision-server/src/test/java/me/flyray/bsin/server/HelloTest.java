package me.flyray.bsin.server;

import me.flyray.bsin.facade.service.BsinIntelligentDecisionEngine;
import me.flyray.bsin.facade.service.HelloService;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.Map;

/**
 * @author ：bolei
 * @date ：Created in 2021/12/22 10:31
 * @description：单元测试示例
 * @modified By：
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class HelloTest {

    @Autowired
    HelloService helloService;

    @Autowired
    private KieContainer kieContainer;

    @Test
    public void hello() throws ClassNotFoundException {
        Map<String ,Object> map = new HashMap<>();
        helloService.add(map);
    }

    @Test
    public void helloDrools(){

        KieSession kieSession = kieContainer.newKieSession("ksession");

        Map map = new HashMap();
        map.put("first",1);
        map.put("second",2);
        map.put("third",3);

        Map map1 = new HashMap();
        map1.put("fourth",4);
        map1.put("fifth",5);
        //工作内存中插入两个map实例
        kieSession.insert(map);
        kieSession.insert(map1);
        kieSession.fireAllRules();
        System.out.println(map.get("serviceName"));
        kieSession.dispose();

    }
}
