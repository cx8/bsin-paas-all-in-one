import { Effect, Reducer, Subscription, history } from 'umi';
import { getAppByUserId, getTenantBaseApp } from '../services/getAppByUserId';
import { getLocalStorageInfo } from '@/utils/localStorageInfo';

export interface AppsState {
  appList: {
    appId: string;
    logo: string;
    appName: string;
    appCode: string;
    url: string;
    appLanguage: string;
    theme: {};
    member: string;
    remark: string;
    updateTime: string;
    props: {
      testProp1: string;
      description: string;
    };
  }[];
  appTotal: number;
}

export interface AppsModelType {
  namespace: 'apps';
  state: AppsState;
  effects: {
    getUserApps: Effect;
  };
  reducers: {
    save: Reducer<AppsState>;
  };
  subscriptions: { setup: Subscription };
}

const AppsModel: AppsModelType = {
  namespace: 'apps',
  state: {
    appList: [],
    appTotal: 0,
  },

  effects: {
    *getUserApps({ payload }, { call, put }) {
      console.log(payload.userType)
      const res = yield call(getAppByUserId, payload);
      if (res.code === '000000') {
        // 如果userType是商户,商户下没应用，查询商户对应租户的产品的base应用
        yield put({
          type: 'save',
          payload: {
            list: res.data,
            appTotal: res.data.length,
          },
        });
      }
    },
  },
  reducers: {
    save(state, { payload }) {
      let appList = payload.list;
      let appTotal = payload.appTotal;
      return { appList, appTotal };
    },
  },
  subscriptions: {
    setup({ dispatch, history }) {
      return history.listen(({ pathname }) => {
        if (pathname != '/login')
          dispatch({
            type: 'getUserApps',
            payload: {
              current: 1,
              pageSize: 8,
              userType: 2
            },
          });
        
      });
    },
  },
};

export default AppsModel;
