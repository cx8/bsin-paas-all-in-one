package me.flyray.bsin.facade.service;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.Map;

/**
 * @author leonard
 * @description Bsin-PaaS openAI接口封装
 * @createDate 2023-04-28 18:41:30
 */

@Path("openAiService")
public interface OpenAiService {

    /**
     * chatGPT对话聊天：基于上下文的对话连天
     */
    @POST
    @Path("chatCompletion")
    @Produces("application/json")
    Map<String, Object> chatCompletion(Map<String, Object> requestMap);


    /**
     * 微信平台，chatGPT对话聊天：基于上下文的对话连天
     */
    @POST
    @Path("wxChatCompletion")
    @Produces("application/json")
    Map<String, Object> wxChatCompletion(Map<String, Object> requestMap);

    /**
     * 余额查询
     */
    @POST
    @Path("creditGrants")
    @Produces("application/json")
    Map<String, Object> creditGrants(Map<String, Object> requestMap);

}
