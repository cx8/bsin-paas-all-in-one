package me.flyray.bsin.server.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

import me.flyray.bsin.server.domain.AiModel;

/**
* @author bolei
* @description 针对表【ai_tenant_ai_model】的数据库操作Mapper
* @createDate 2023-04-25 18:40:35
* @Entity generator.domain.AiTenantAiModel
*/
@Mapper
@Repository
public interface AiModelMapper {

    AiModel selectByCode(String code);

    void insert(AiModel aiModel);

    void deleteById(String serialNo);

    void updateById(AiModel aiModel);

    AiModel selectBySerialNo(String serialNo);

    List<AiModel> selectPageList(@Param("tenantId") String tenantId, @Param("name") String name, @Param("code") String code);

}
